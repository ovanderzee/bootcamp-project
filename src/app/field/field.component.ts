import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.scss']
})
export class FieldComponent implements OnInit {
  @Input() x?:number;
  @Input() y?:number;
  @Input() m?: number;
  @Output() onClick = new EventEmitter();
  show: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  /*
    Handle state change
  */
  update(): void {
    this.show = !this.show;
    this.onClick.emit();
  }
}
