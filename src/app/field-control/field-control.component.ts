import {Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

@Component({
  selector: 'app-field-control',
  templateUrl: './field-control.component.html',
  styleUrls: ['./field-control.component.scss']
})
export class FieldControlComponent implements OnInit {
  @Input() m?: number;
  @Input() n?: number;
  @Output() horizontal = new EventEmitter<number>();
  @Output() vertical = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  /*
    Output new number of columns
  */
  mutateCols(increment: number):void {
    if (typeof this.m === 'number') {
      this.m = Math.max(this.m + increment, 0);
      this.horizontal.emit(this.m);
    }
  }

  /*
    Output new number of rows
  */
  mutateRows(increment: number):void {
    if (typeof this.n === 'number') {
      this.n = Math.max(this.n + increment, 0);
      this.vertical.emit(this.n);
    }
  }

}
