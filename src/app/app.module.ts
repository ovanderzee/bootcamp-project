import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FieldComponent } from './field/field.component';
import { TimesPipe } from './times.pipe';
import { FieldControlComponent } from './field-control/field-control.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FieldComponent,
    TimesPipe,
    FieldControlComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
