import {
  Component,
  QueryList,
  ViewChildren
} from '@angular/core';
import {FieldComponent} from "./field/field.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChildren(FieldComponent) fieldChildren!: QueryList<FieldComponent>;
  title: string = 'bootcamp-project';
  m: number = 4;
  n: number = 2;
  openCount: number = 0;

  /*
    When clicked or referenced
    Do count analysis
  */
  updateCount(): void {
    const openedChildren = this.fieldChildren
      .filter((field) => field.show);
    this.openCount = openedChildren.length;
  }

  /*
    Change the horizontal size of the grid
    Redo count analysis
  */
  mutateHorizontal(m: number): void {
    this.m = m;
    setTimeout(this.updateCount.bind(this));
  }

  /*
    Change the vertical size of the grid
    Redo count analysis
  */
  mutateVertical(n: number): void {
    this.n = n;
    setTimeout(this.updateCount.bind(this));
  }
}
